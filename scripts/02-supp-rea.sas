%oracle_create(REA_Z_SUPP_REA)

   select ETA_NUM, RSA_NUM, NBR_SUP_REA, AGE_ANN, DGN_PAL, GRG_GHM
   from T_MCO20B
   where regexp_like (ETA_NUM, '^(02|59|60|62|80)')
   and   NBR_SUP_REA > 0

%oracle_end()

%count_lines(REA_Z_SUPP_REA)
